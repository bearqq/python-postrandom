# coding: utf-8
#15-05-15
#note
"""
post:
def target_url():return 'http://www.baidu.com'
def postdata():return 'username=%s&password=%s' % (rand_max(6),rand_123(5,8))

get:
def target_url():return 'http://www.baidu.com?s=%s' % rand_abc(10)
def postdata():return None

available functions:
rand_abc,rand_123,rand_Abc,rand_Ab3,rand_max
"""
#imports
from randabc import *

#some definitions:
sleeptime=0.5 	#in second
def target_url():return 'http://www.baidu.com'
def postdata():return 'username=%s&password=%s' % (rand_max(6),rand_123(5,8))
retstr=u''	#threats continue if retstr is in return-page
totalthreat=2
runonce=False
useproxy=1
proxyaddr='http://127.0.0.1:8080'
debug=1

#some global inits:
import os,sys
import threading
import urllib2, urllib
import time
flag=1
if sys.getdefaultencoding() != 'utf8':
	reload(sys)
	sys.setdefaultencoding( "utf-8" )

#functions
def randomip():#ip random
	return str(random.randint(0,254))+'.'+str(random.randint(0,254))+'.'+str(random.randint(0,254))+'.'+str(random.randint(0,254))

def codeiss(data):#decode
	try:
		return data.decode("GBK")
	except:
		return data.decode("UTF-8")

def fuckurl(url,postdata=None,cookie=''):#fetch from url and then decode
	if debug:
		print url,postdata
	if useproxy:
		proxy_handler=urllib2.ProxyHandler({'http':proxyaddr})
		opener1 = urllib2.build_opener(proxy_handler)
	else:
		opener1 = urllib2.build_opener()
	req = urllib2.Request(url,postdata)
	req.add_header("User-Agent","Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:27.0) Gecko/20100101 Firefox/27.0")
	req.add_header("Cookie",cookie)
	ip=randomip()
	req.add_header("X-FORWARDED-FOR",ip)
	req.add_header("VIA",ip)
	req.add_header("CLIENT-IP",ip)
	req.add_header('Accept', "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp")#
	req.add_header('Accept-Encoding', "*")

	try:
		retreq=opener1.open(req)
	except:# IOError,e:
		print 'Net work Error!\n'#+' '+str(e.reason)
		return ''
	else:
		if  retreq.headers.get('Content-Encoding') == 'gzip':
			return codeiss(gzip.GzipFile(fileobj=StringIO(retreq.read())).read())
		else:
			return codeiss(retreq.read())
	
class fuckit(threading.Thread):
	def __init__(self,tk):
		threading.Thread.__init__(self)
		self.i=0
		self.tk=tk
	
	def count(self):
		self.i=self.i+1
		return self.i
		
	def run(self):
		global flag
		while flag:
			try:
				print 'Threat: %d \tCount: %d' % (self.tk,self.count())
				r=fuckurl(target_url(), postdata=postdata())
				if debug:
					print r
				if retstr:
					if retstr not in r:
						flag=0
				time.sleep(sleeptime)
			except:
				pass
			if runonce:
				flag=False

def main():
	threats=[]
	for i in xrange(totalthreat):
		threats.append(fuckit(i))
	for i in threats:
		i.start()
	for i in threats:
		i.join()

if __name__ == '__main__':
	main()