import random

"""
	''.join(map(lambda xx:(hex(ord(xx))[2:]),os.urandom(16)))
	
	str="" 
	for i in range(len): 
		str+=(random.choice("abcdefghijklmnopqrstuvwxyz")) 
	return str
"""

def rand_abc(fr,to=None):
	return ''.join([random.choice("abcdefghijklmnopqrstuvwxyz") for i in range(random.randint(fr,to) if to else fr)])

def rand_123(fr,to=None):
	return ''.join([random.choice("1234567890") for i in range(random.randint(fr,to) if to else fr)])

def rand_Abc(fr,to=None):
	return ''.join([random.choice("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ") for i in range(random.randint(fr,to) if to else fr)])

def rand_Ab3(fr,to=None):
	return ''.join([random.choice("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890") for i in range(random.randint(fr,to) if to else fr)])

def rand_max(fr,to=None):
	return ''.join([random.choice("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_.\\/!@#$%^*(){}-=+|'\";:<>?`~") for i in range(random.randint(fr,to) if to else fr)])