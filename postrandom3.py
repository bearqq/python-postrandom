# coding: utf-8
#21-07-29
#note
"""
post:
def target_url():return 'http://www.baidu.com'
def postdata():return 'username=%s&password=%s' % (rand_max(6),rand_123(5,8))

get:
def target_url():return 'http://www.baidu.com?s=%s' % rand_abc(10)
def postdata():return None

available functions:
rand_abc,rand_123,rand_Abc,rand_Ab3,rand_max
"""
#imports
from randabc import *

#some definitions:
sleeptime=0.5 	#in second
def target_url():return 'http://www.baidu.com'
def postdata():return 'username=%s&password=%s' % (rand_max(6),rand_123(5,8))
retstr=u''	#threats continue if retstr is in return-page
totalthreat=3
runonce=0
useproxy=0
proxies = {
    'http': 'http://127.0.0.1:8080',
    'https': 'http://127.0.0.1:8080',
}
debug=1

#some global inits:
import os,sys
import threading
import requests
import time,random
flag=1

#functions
def randomip():#ip random
    return str(random.randint(0,254))+'.'+str(random.randint(0,254))+'.'+str(random.randint(0,254))+'.'+str(random.randint(0,254))

def fuckurl(url,postdata=None,cookie=''):#fetch from url and then decode
    headers={"User-Agent":"Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:27.0) Gecko/20200101 Firefox/85.0"}
    headers['Cookie']=cookie
    ip=randomip()
    headers["X-FORWARDED-FOR"]=ip
    headers["VIA"]=ip
    headers["CLIENT-IP"]=ip
    headers['Accept']= "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp"
    headers['Accept-Encoding']="*"

    try:
        if useproxy:
            if postdata:
                r=requests.post(url,data=postdata,proxies=proxies,headers=headers)
            else:
                r=requests.get(url,proxies=proxies,headers=headers)
        else:
            if postdata:
                r=requests.post(url,data=postdata,headers=headers)
            else:
                r=requests.get(url,headers=headers)
    except Exception as e:
        print(e)
        return 'network error'

    try:
        return r.text
    except:
        return r.status_code
    
class fuckit(threading.Thread):
    def __init__(self,tk):
        threading.Thread.__init__(self)
        self.i=0
        self.tk=tk
    
    def count(self):
        self.i=self.i+1
        return self.i
        
    def run(self):
        global flag
        while flag:
            try:
                print('Threat: %d \tCount: %d' % (self.tk,self.count()))
                r=fuckurl(target_url(), postdata=postdata())
                if debug:
                    print(r)
                if retstr:
                    if retstr not in r:
                        flag=0
                time.sleep(sleeptime)
            except Exception as e:
                print(e)
            if runonce:
                flag=False

def main():
    threats=[]
    for i in range(totalthreat):
        threats.append(fuckit(i))
    for i in threats:
        i.start()
    for i in threats:
        i.join()

if __name__ == '__main__':
    main()